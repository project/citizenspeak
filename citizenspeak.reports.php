<?php

/**
 * @file
 * Reporting functionality for the CitizenSpeak module
 */ 

/**
 * Display the form for getting reports, or a report for a node
 *
 * @param $nid
 *   A node ID
 */
function citizenspeak_report($node) {
  $params = array('nid' => $node->nid);
  $nid = $node->nid;
  if (!isset($_GET['format'])) {
    $params = array_merge($params, 
      db_fetch_array(db_query("SELECT COUNT(*) AS total_emails, MAX(sent_at) AS last_email FROM {citizenspeak_participants} WHERE nid = %d", $nid)));

    list($params['total_zips'], $params['zips']) = _citizenspeak_get_zips($nid);
    $output = theme('citizenspeak_statistics_page', $params);
    echo theme('page', $output);
  } 
  else {
    $participants = array();
    $participants_result = db_query("SELECT * FROM {citizenspeak_participants} WHERE nid = %d", $nid);
    while ($row = db_fetch_array($participants_result)) {
      $participants[] = $row;
    }

    switch($_GET['format']) {
      case "dbimport":
        $report_theme = 'citizenspeak_report_dbimport';
        $content_type = 'text/tab-separated-values';
        $ext          = 'tab';
        break;

      case "excel":
        $report_theme = 'citizenspeak_report_excel';
        $content_type = 'application/msexcel';
        $ext          = 'xls';
        break;

      case "text":
      default:
        $report_theme = 'citizenspeak_report_text';
        $content_type = 'text/plain';
        $ext          = 'txt';
        break;
    }

    header("Content-Type: $content_type; name=report.$ext");
    header("Content-Disposition: attachment; filename=report.$ext");
    print theme($report_theme, $node, $participants);;
  }
}

/**
 * Format a collection of participants as a tab separated text file
 *
 * @param $node
 *   A CitizenSpeak campaign node
 *
 * @param $participants
 *   An array of CitizenSpeak participant row arrays
 *
 * @ingroup themeable
 */
function theme_citizenspeak_report_text($node, $participants) {
  $output = t("Campaign Report For %title
Created %created_date

Full Name	Organization	Address	City	State	ZIP	Email	Phone	Fax	PS	Sent
", array(
  '%title'        => $node->title,
  '%created_date' => format_date($node->created)));

  foreach ($participants as $row) {
    $output .= join("\t", array($row['name'], $row['organization'], $row['address'], $row['city'], $row['state'], $row['zip'], $row['email'], $row['phone'], $row['fax'], $row['personal_statement'], $row['sent_at']))."\n";
  }

  return $output;
}

/**
 * Format a collection of participants as a tab separated text file
 *
 * @param $node
 *   A CitizenSpeak campaign node
 *
 * @param $participants
 *   An array of CitizenSpeak participant row arrays
 *
 * @ingroup themeable
 */
function theme_citizenspeak_report_excel($node, $participants) {
  $output = t("Campaign Report For %title
Created %created_date

Full Name	Organization	Address	City	State	ZIP	Email	Phone	Fax	PS	Sent
", array(
  '%title'        => $node->title,
  '%created_date' => format_date($node->created)));

  foreach ($participants as $row) {
    $output .= join("\t", array($row['name'], $row['organization'], $row['address'], $row['city'], $row['state'], '=("'. $row['zip']. '")', $row['email'], $row['phone'], $row['fax'], $row['personal_statement'], $row['sent_at']))."\n";
  }

  return $output;
}

/**
 * Format a collection of participants as a tab separated text file
 *
 * @param $node
 *   A CitizenSpeak campaign node
 *
 * @param $participants
 *   An array of CitizenSpeak participant row arrays
 *
 * @ingroup themeable
 */
function theme_citizenspeak_report_dbimport($node, $participants) {
  $output = t("Full Name	Organization	Address	City	State	ZIP	Email	Phone	Fax	PS	Sent\n");

  foreach ($participants as $row) {
    $output .= join("\t", array($row['name'], $row['organization'], $row['address'], $row['city'], $row['state'], $row['zip'], $row['email'], $row['phone'], $row['fax'], $row['personal_statement'], $row['sent_at'])). "\n";
  }

  return $output;
}
