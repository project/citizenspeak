CitizenSpeak Module

The CitizenSpeak module allows users to create and participate in email action
campaigns.  It was developed for http://www.citizenspeak.org/

Users are able to create a campaign form that will send emails to the target 
of the campaign.  The user that created the campaign can also track 
participation in the campaign.

