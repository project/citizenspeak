<?php

/**
 * @file
 * Theme functions for the CitizenSpeak module
 */


function citizenspeak_theme() {
  return array(
      'citizenspeak_message' => array(
         'arguments' => array('node' => NULL, 'participant' => NULL),
      ),

      'citizenspeak_message_preview' => array(
         'arguments' => array('node' => NULL),
      ),

      'citizenspeak_participate_display' => array(
         'arguments' => array('node' => NULL),
      ),

      'citizenspeak_thank_you' => array(
         'arguments' => array('node' => NULL, 'user' => NULL),
      ),

      'citizenspeak_statistics_page' => array(
         'arguments' => array('params' => NULL),
      ),

      'citizenspeak_message_headers' => array(
         'arguments' => array('node' => NULL, 'participant' => NULL),
      ),

      'citizenspeak_debug_page' => array(
         'arguments' => array('node' => NULL, 'message' => NULL, 'headers' => NULL),
      ),

      'citizenspeak_report_text' => array(
         'arguments' => array('node' => NULL, 'message' => NULL, 'participants' => NULL),
      ),

      'citizenspeak_report_excel' => array(
         'arguments' => array('node' => NULL, 'message' => NULL, 'participants' => NULL),
      ),

      'citizenspeak_report_dbimport' => array(
         'arguments' => array('node' => NULL, 'message' => NULL, 'participants' => NULL),
      ),
  );
}

/**
 * Display the form to participate in a campaign
 *
 * @param $node 
 *   A CitizenSpeak campaign node
 * @ingroup themeable
 */
function theme_citizenspeak_participate_display($node) {
  $edit = $_POST['edit'];

  $output = '';
  $output .= '<div class="citizenspeak_participate">';
  $output .= theme('citizenspeak_message_preview', $node);

  $output .= '</div>';
  return $output;
} // function theme_citizenspeak_form_display

/**
 * Display what the emailed form will look like
 *
 * @param $node
 *   A CitizenSpeak campaign node
 * @return A preview of the message CitizenSpeak will send
 * @ingroup themeable
 */
function theme_citizenspeak_message_preview($node) {
  $participant = new StdClass();
  $participant->name = t('Your Name');
  $participant->organization = t('Your Organization');
  $participant->email = 'you@example.com';
  $participant->address = t('123 Your St.');
  $participant->city = t('Yousville');
  $participant->state = t('YO');
  $participant->zip = t('12345');
  $participant->phone = t('(123)456-7890');
  $participant->fax = t('(123)456-7890x123');
  $participant->personal_statement = t('Your Personal Statement');
  
  $message = theme("citizenspeak_message", $node, $participant);
  $headers = theme("citizenspeak_message_headers", $node, $participant);  
  
  $output = '<div id="citizenspeak_message_preview"> ';
  $output .= '<tt>';
  $output .= t("From: ") ."$participant->name &lt;$participant->email&gt;<br />";
  if (variable_get('citizenspeak_show_to', 1)) {
    $output .= t("To: ") . htmlentities($node->email_recipients) ."<br />";
  }
  $output .= t("Subject: ") . htmlentities($node->title) ."<br /><br />";
  $output .= nl2br(htmlentities($message));
  $output .= '</tt>';
  $output .= '</div>';
  return theme_box('Message Preview', $output);
} // function theme_citizenspeak_block_popular


/**
 * Display the thank you page after the campaign has been participated in
 *
 * @param $node
 *   A CitizenSpeak campaign node
 * @param $user
 *   The owner of the node
 * @ingroup themeable
 */
function theme_citizenspeak_thank_you($node, $user) {
//  $user = user_load(array("uid" => $node->uid));
  drupal_set_title(t('Thank You'));

  $output = '<div id="citizenspeak_thank_you">';
  $output .= t('<p>Thank you for supporting our campaign.  Your email is currently being sent to the individuals identified by '.theme('username', $user).'. You can also be of great help to the campaign simply by passing the word along. All it takes is a brief letter to your e-mail circle.</p>');
  
  // Add action list
  $tell_a_friend_text = t("Dear Friend,
  
Please join me in supporting the ".$node->title." campaign. It's easy, just go to ".url('node/'. $node->nid, array('absolute' => true))." and make your voice heard.

Thank you,
");
  $tell_a_friend_link = "mailto:?subject=". rawurlencode($node->title) ."&body=". rawurlencode($tell_a_friend_text);
  $actions = array(t('<a href="'.$tell_a_friend_link.'">Tell a friend</a>'));
    
  if ($user->request_memberships) {
    $membership_link =  sprintf('https://www.paypal.com/xclick/business=%s&item_name=%s&amount=%.2f&no_note=1&no_shipping=1&tax=0&currency_code=USD', urlencode($user->membership_paypal_id.t(" membership dues")), urlencode($user->username), urlencode($user->membership_paypal_amount));
    $actions[] = t('Become a member.  <a href="'.$membership_link.'">Join for $'.$user->membership_paypal_amount.' per year.</a>');
  }

  if ($user->request_donations) {
    $donation_link = sprintf('https://www.paypal.com/xclick/business=%s&item_name=%s&no_note=1&no_shipping=1&tax=0&currency_code=USD', urlencode($user->donation_paypal_id), urlencode($user->username.t(' donation')));
    $actions[] = t('<a href="'.$donation_link.'">Make a donation.</a>');
  }

  if ($user->request_volunteers) {
    $actions[] = t('<a href="mailto:'.$user->volunteer_email.'">Become a volunteer</a>');
  }

  $output .= theme('item_list', $actions);

  if ($user->include_additional_text) {
    $output .= "<p>$user->additional_text</p>";
  }

  if ($user->request_feedback) {
    $output .= t('<p>If you have any questions, feel free to contact <a href="mailto:'.$user->feedback_email.'">'.$user->feedback_email.'</a>.</p>');
  }

  $output .= t("<p>Sincerely,<br /><strong>".theme('username', $user)."</strong></p>");
  $output .= '</div>';
  return $output;
}

/**
 * Display the participation statistics page
 *
 * @param $params
 *   An associative array with the following keys:
 *   total_emails
 *   total_zips
 *   last_email
 *   zips
 * @ingroup themeable
 */
function theme_citizenspeak_statistics_page($params) {
  drupal_set_title(t('Campaign Statistics'));

  $output = '<div id="citizenspeak_statistics">';
  $output .= t("<strong>Total Emails Sent:</strong> %count<br />", array("%count" => $params['total_emails']));
  $output .= t('<strong>Unique ZIP Codes:</strong> %count<br />', array("%count" => $params['total_zips']));
  $output .= t('<strong>Last Email Sent:</strong> %count<br />', array("%count" => $params['last_email']));

  $output .= t("<p>Download reports with all your campaign participants' contact information, including name, organization, address, email address and personal statement.  With campaign reports you can:<ul><li>Expand your list of supporters</li><li>Let supporters know they’re not alone.</li><li>Identify compelling personal statements.</li></ul>Please respect your campaign participants’ privacy by getting consent before making public their contact information and statements.</p>");
  $output .= t("<p>To download a report, choose a file format below.</p>");

  $citizenspeak_stats_links = array();
  $citizenspeak_stats_links['text-file'] = array(
    'title' => t('Text file'),
    'href' => 'node/'. arg(1) .'/report',
    'attributes' => array(),
    'query' => 'format=text',
  );

  $citizenspeak_stats_links['excel-file'] = array(
    'title' => t('Excel file'),
    'href' => 'node/'. arg(1) .'/report',
    'attributes' => array(),
    'query' => 'format=excel',
  );

  $citizenspeak_stats_links['dbimport'] = array(
    'title' => t('dbimport'),
    'href' => 'node/'. arg(1) .'/report',
    'attributes' => array(),
    'query' => 'format=dbimport',
  );
  
  $output .= theme('links', $citizenspeak_stats_links);

  $output .= t('<h2>ZIP Code Summary</h2>');
  $output .= theme_table(array(t('ZIP Code'), t('Emails')), $params['zips']);

  $output .= '</div>';
  return $output;
}

/**
 * The content of an email to be sent
 *
 * @param $node
 *   A CitizenSpeak campaign node
 * @param $participant
 *   A CitizenSpeak campaign participant
 * @ingroup themeable
 */
function theme_citizenspeak_message($node, $participant) {
  // campaign_format is whether to include the personal statement at the top
  if ($node->campaign_format) {
    $output .= " $participant->personal_statement
";
  }

  $output .= "
$node->email_message

$participant->name
$participant->organization
$participant->address
$participant->city, $participant->state $participant->zip";
  if ($participant->phone) {
    $output .= "
Phone: $participant->phone";
  }

  if ($participant->fax) {
    $output .= "
Fax: $participant->fax";
  }

  if (!$node->campaign_format) {
    $output .= "
p.s.
$participant->personal_statement";
  }
  $sig = t(variable_get('citizenspeak_signature', ""), array("%title" => $node->title, "%url" => url("node/". $node->nid, array('absolute' => true)), "%nid" => $node->nid));

  if (strlen($sig)) {
    $output .= "
-- 
$sig";
  }

  return $output;
}

/**
 * Additional headers to add to emails
 *
 * @param $node
 *   A CitizenSpeak campaign node
 * @param $participant
 *   A CitizenSpeak campaign participant
 * @ingroup themeable
 */

function theme_citizenspeak_message_headers($node, $participant) {
  return "From: {$participant->name} <{$participant->email}>
X-Citizenspeak-Id: $node->nid";
}

/**
 * Debugging Page
 *
 * @param $node
 *   A CitizenSpeak campaign node
 * @param $message
 *   The body of a message, from theme_citizenspeak_message()
 * @param $headers
 *   Additional headers, from theme_citizenspeak_message_headers()
 */
function theme_citizenspeak_debug_page($node, $message, $headers) {
  $output = t('<p>Message not sent because the system is in debugging mode.  Use <a href="%settings_url">the CitizenSpeak settings</a> to turn of debugging mode.  The email that would have been sent is below.</p>', array("%settings_url" => url("admin/settings/citizenspeak")));
  $output .= '<pre>';
  $output .= htmlentities("{$headers}\n");
  $output .= htmlentities("To: {$node->email_recipients}\n");
  $output .= htmlentities("Subject: {$node->title}\n");
  $output .= htmlentities("\n{$message}\n");
  $output .= '</pre>';
  $output .= l(t('Continue to Thank You page'), "node/". $node->nid."/thank_you");
  drupal_set_message($output);
} 
