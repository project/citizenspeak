<?php

/**
 * @file
 * Functions for the CitizenSpeak module's hook_user() implementation
 */

/**
 * A set of user information categories is requested.
 *
 * @param &$edit
 *   The array of form values submitted by the user.
 * @param &$user 
 *   The user object on which the operation is being performed.
 * @param $category 
 *   The active category of user information being edited.
 * @return 
 *  A linear array of associative arrays. These arrays have keys: 
 *  - "name": The internal name of the category. 
 *  - "title": The human-readable, localized name of the category. 
 *  - "weight": An integer specifying the category's sort  ordering.
 */
 
function citizenspeak_user_categories(&$edit, &$account, $category = false) {
  global $user;
  // TODO: make weight customizable in admin interface
  if (user_access('customize thank you page', $user)) {
    return array(array('name' => 'citizenspeak_thank_you', 'title' => t('campaign thank you pages'), 'weight' => 0));
  }
}

/**
 * The user account is being deleted. The module should remove its custom 
 * additions to the user object from the database.
 *
 * @param &$edit
 *   The array of form values submitted by the user.
 * @param &$user 
 *   The user object on which the operation is being performed.
 * @param $category 
 *   The active category of user information being edited.
 */

function citizenspeak_user_delete(&$edit, &$user, $category = false) {
  db_query("DELETE FROM {citizenspeak_thankyou_options} WHERE uid = %d", $user->uid);
}

/**
 * The user account edit form is about to be displayed. The module should 
 * present the form elements it wishes to inject into the form.
 *
 * @param &$edit
 *   The array of form values submitted by the user.
 * @param &$user 
 *   The user object on which the operation is being performed.
 * @param $category 
 *   The active category of user information being edited.
 * @return
 *   A linear array of form groups for the specified category. Each group 
 *   is represented as an associative array with keys: 
 *   - "title": The human-readable, localized name of the group. 
 *   - "data": A string containing the form elements to display. 
 *   - "weight": An integer specifying the group's sort ordering. 
 */

function citizenspeak_user_form(&$edit, &$user, $category = false) {
  if ($category == "citizenspeak_thank_you" && user_access('customize thank you page', $user)) {
    $form = array();
    
    $form['use_redirect_false']['use_redirect'] = array(
      '#name' => 'edit[use_redirect]',
      '#type' => 'radio',
      '#title' => t('Customize standard page'),
      '#return_value' => 0,
      '#default_value' => $user->use_redirect,
    );
    $form['use_redirect_false'][] = array(
      '#type' => 'markup',
      '#value' => '<div style="margin-left: 2em">',
    );

    $form['use_redirect_false']['request_volunteers'] = array(
      '#type' => 'checkbox',
      '#title' => t('Volunteer signup'),
      '#return_value' => 1,
      '#default_value' => $user->request_volunteers,
    );
    $form['use_redirect_false']['volunteer_email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email to'),
      '#default_value' => $user->volunteer_email,
      '#size' => 25,
      '#maxlength' => 255,
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );

    $form['use_redirect_false']['request_donations'] = array(
      '#type' => 'checkbox',
      '#title' => t('Donation link'),
      '#return_value' => 1,
      '#default_value' => $user->request_donations,
    );
    $form['use_redirect_false']['donation_paypal_id'] = array(
      '#type' => 'textfield',
      '#title' => t('PayPal ID'),
      '#default_value' => $user->donation_paypal_id,
      '#size' => 25,
      '#maxlength' => 255,
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );

    $form['use_redirect_false']['request_memberships'] = array(
      '#type' => 'checkbox',
      '#title' => t('Membership link'),
      '#return_value' => 1,
      '#default_value' => $user->request_memberships,
    );
    $form['use_redirect_false']['membership_paypal_id'] = array(
      '#type' => 'textfield',
      '#title' => t('PayPal ID'),
      '#default_value' => $user->membership_paypal_id,
      '#size' => 25,
      '#maxlength' => 255,
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );
    $form['use_redirect_false']['membership_paypal_amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Dues (USD)'),
      '#default_value' => $user->membership_paypal_amount,
      '#size' => 6,
      '#maxlength' => 10,
      '#description' => t('(yearly)'),
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );

    $form['use_redirect_false']['include_additional_text'] = array(
      '#type' => 'checkbox',
      '#title' => t('Additional text'),
      '#return_value' => 1,
      '#default_value' => $user->include_additional_text,
    );
    $form['use_redirect_false']['additional_text_language'] = array(
      '#type' => 'select',
      '#title' => '',
      '#default_value' => $user->additional_text_language,
      '#options' => drupal_map_assoc(array(t('English'), t('Spanish'))),
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );
    $form['use_redirect_false']['additional_text'] = array(
      '#type' => 'textarea',
      '#title' => '',
      '#default_value' => $user->additional_text,
      '#cols' => 60,
      '#rows' => 6,
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );

    $form['use_redirect_false']['request_feedback'] = array(
      '#type' => 'checkbox',
      '#title' => t('Feedback'),
      '#return_value' => 1,
      '#default_value' => $user->request_feedback,
    );
    $form['use_redirect_false']['feedback_email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email to'),
      '#default_value' => $user->feedback_email,
      '#size' => 25,
      '#maxlength' => 255,
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );

    $form['use_redirect_false'][] = array(
      '#type' => 'markup',
      '#value' => '</div>',
    );


    $form['use_redirect_true']['use_redirect'] = array(
      '#name' => 'edit[use_redirect]',
      '#type' => 'radio',
      '#title' => t('Use your page'),
      '#return_value' => 1,
      '#default_value' => $user->use_redirect,
    );

    $form['use_redirect_true']['redirect_url'] = array(
      '#type' => 'textfield',
      '#default_value' => $user->redirect_url,
      '#size' => 25,
      '#maxlength' => 255,
      '#description' => t('Your full web address'),
      '#prefix' => '<div style="margin-left: 2em">',
      '#suffix' => '</div>',
    );

    return $form;
  } 
}

/**
 * The user account is being added. The module should save its custom 
 * additions to the user object into the database and set the saved fields 
 * to NULL in $edit.
 *
 * @param &$edit
 *   The array of form values submitted by the user.
 * @param &$user 
 *   The user object on which the operation is being performed.
 * @param $category 
 *   The active category of user information being edited.
 */

function citizenspeak_user_insert(&$edit, &$user, $category = false) {
  db_query("INSERT INTO {citizenspeak_thankyou_options} (uid) VALUES (%d)", $user->uid);
}

/**
 * The user account is being changed. The module should save its custom 
 * additions to the user object into the database and set the saved fields 
 * to NULL in $edit.
 *
 * @param &$edit
 *   The array of form values submitted by the user.
 * @param &$user 
 *   The user object on which the operation is being performed.
 * @param $category 
 *   The active category of user information being edited.
 */

function citizenspeak_user_update(&$edit, &$user, $category = false) {
  if ($category == "citizenspeak_thank_you" && user_access('customize thank you page', $user)) {
    db_query('DELETE FROM {citizenspeak_thankyou_options} WHERE uid = %d', $user->uid);
    db_query("INSERT INTO {citizenspeak_thankyou_options} (use_redirect, redirect_url, request_volunteers, volunteer_email, request_donations, donation_paypal_id, request_memberships, membership_paypal_id, membership_paypal_amount, include_additional_text, additional_text_language, additional_text, request_feedback, feedback_email, uid) VALUES (%d, '%s', %d, '%s', %d, '%s', %d, '%s', %f, %d, '%s', '%s', %d, '%s', %d)", $edit['use_redirect'], $edit['redirect_url'], $edit['request_volunteers'], $edit['volunteer_email'], $edit['request_donations'], $edit['donation_paypal_id'], $edit['request_memberships'], $edit['membership_paypal_id'], $edit['membership_paypal_amount'], $edit['include_additional_text'], $edit['additional_text_language'], $edit['additional_text'], $edit['request_feedback'], $edit['feedback_email'], $user->uid);
  }
}

/**
 * The user account is about to be modified. The module should validate its 
 * custom additions to the user object, registering errors as necessary.
 *
 * @param &$edit
 *   The array of form values submitted by the user.
 * @param &$user 
 *   The user object on which the operation is being performed.
 * @param $category 
 *   The active category of user information being edited.
 */

function citizenspeak_user_validate(&$edit, &$user, $category = false) {
  if ($category == "citizenspeak_thank_you" && user_access('customize thank you page', $user)) {
    if ($edit['volunteer_email'] && !valid_email_address($edit['volunteer_email'])) {
      form_set_error('volunteer_email', 'You must provide a valid email address');
    }
    
    if ($edit['feedback_email'] && !valid_email_address($edit['feedback_email'])) {
      form_set_error('feedback_email', 'You must provide a valid email address');
    }
    
    if ($edit['donation_paypal_id'] && !valid_email_address($edit['donation_paypal_id'])) {
      form_set_error('donation_paypal_id', 'You must provide a valid email address');
    }
    
    if ($edit['membership_paypal_id'] && !valid_email_address($edit['membership_paypal_id'])) {
      form_set_error('membership_paypal_id', 'You must provide a valid email address');
    }
  
    if ($edit['redirect_url'] && !valid_url($edit['redirect_url'])) {
      form_set_error('redirect_url', 'You must provide a valid web address');
    }
  }
}
