<?php

/**
 * @file
 * Helper functions for the CitizenSpeak module
 */

function _citizenspeak_get_zips($nid) {
  $zip_result = db_query("SELECT zip, COUNT(*) AS from_zip FROM {citizenspeak_participants} WHERE nid = %d GROUP BY zip ORDER BY from_zip DESC", $nid);
  $distinct_zips = 0; // = db_num_rows($zip_result);
  $zips = array();

  while ($row = db_fetch_array($zip_result)) {
    $zips[] = $row;
    $distinct_zips++;
  }
  
  $total = db_result(db_query("SELECT COUNT(*) FROM {citizenspeak_participants} WHERE nid = %d", $nid));
  
  return array($distinct_zips, $zips, $total);  
}

/**
 * Split email addresses into an array
 *
 * @param $emails One or more email addresses separated with commas and spaces
 * @returns array of email addresses
 */
function _citizenspeak_split_emails($emails) {
  return preg_split('/[\s,]+/', $emails);
}

function citizenspeak_send_petition_form_validate($form_id, &$form_state) {
  if ($form_state['values']['name'] == '') {
    form_set_error('name', t('You must enter your name'));
  }
  
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('You must provide a valid email address'));
  }

  if (variable_get('citizenspeak_require_address', 1)) {
    if ($form_state['values']['address'] == '') {
      form_set_error('name', t('You must enter your address'));
    }  

    if ($form_state['values']['city'] == '') {
      form_set_error('city', t('You must enter your city'));
    }

    if ($form_state['values']['state'] == '') {
      form_set_error('state', t('You must enter your state'));
    }

    if (!_citizenspeak_valid_zipcode($form_state['values']['zip'])) {
      form_set_error('zip', t('You must provide a 5 digit ZIP code'));
    }
  }
}

function _citizenspeak_valid_zipcode($zip) {
  return preg_match('/^\d{5}$/', $zip);
}

function _citizenspeak_send_reminders($node) {
  $send_15  = variable_get('citizenspeak_remind_15', false);
  $send_50  = variable_get('citizenspeak_remind_50', false);
  $send_100 = variable_get('citizenspeak_remind_100', false);
  // Skip the DB hit if responses aren't sent
  if ($send_15 or $send_50 or $send_100) {
    $responses = db_result(db_query('SELECT COUNT(*) AS responses FROM {citizenspeak_participants} WHERE nid = %d', $node->nid));

    // Send the message if we are at a threshold
    if (($responses == 15 and $send_15) or
        ($responses == 50 and $send_50) or
        ($responses == 100 and $send_100)) {
      $owner = user_load(array("uid" => $node->uid));
      $headers = t('From: %site_name <%site_email>', array("%site_name" => variable_get("site_name", ""), "%site_email" => variable_get("site_mail", "")));
      $subject = variable_get('citizenspeak_reminder_subject', CITIZENSPEAK_DEFAULT_REMINDER_SUBJECT);
      $body = t(variable_get('citizenspeak_reminder_template', CITIZENSPEAK_DEFAULT_REMINDER_TEMPLATE), array("%title" => $node->title, "%count" => $responses, "%url" => url("node/$node->nid/report", array('absolute' => true))));
      mail($owner->mail, $subject, wordwrap($body, 70), $headers);
    }
  }
}
