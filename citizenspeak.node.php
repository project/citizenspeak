<?php

/**
 * @file
 * Node module functions for the CitizenSpeak module
 */
 
 /**
  * Implementation of hook_access().
  */
function citizenspeak_access($op, $node) {
  global $user;

  if ($op == 'create') {
    // Only users with permission to do so may create this node type.
    return user_access('create campaigns');
  }
  
  // Users who create a node may edit or delete it later, assuming they have the
  // necessary permissions.
  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit own campaigns') && ($user->uid == $node->uid)) {
      return true;
    } 
  }

  if ($op == "collect contact information") {
    if (user_access('collect contact information') && ($user->uid == $node->uid)) {
      return true;
    }
  }
  
  return null;
} // function citizenspeak_access

/**
 * Implementation of hook_delete().
 */
function citizenspeak_delete($node) {
  db_query("DELETE FROM {citizenspeak_campaigns} WHERE nid = %d", $node->nid);
} // function citizenspeak_delete

/**
 * Implementation of hook_form().
 */
function citizenspeak_form($node) {
  $form = array();
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $node->title,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
 );

 // Now we define the form elements specific to our node type. 
 $form['email_message'] = array(
   '#type' => 'textarea',
   '#title' => t('Email Message'),
   '#default_value' => $node->email_message,
   '#cols' => 60,
   '#rows' => 20,
   '#description' => t('Note: This text will be followed by the participant\'s contact information.'),
   '#required' => TRUE,
  );
  $form['email_recipients'] = array(
    '#type' => 'textarea',
    '#title' => t('Send Message To'),
    '#default_value' => $node->email_recipients,
    '#cols' => 60,
    '#rows' => 6,
    '#description' => t('Use a comma or space to separate addresses. Make sure the above email addresses are valid to avoid campaign suspension.'),
    '#required' => TRUE,
  );
  $form['campaign_format'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put personal statements at top of letter'),
  );

  return $form;
} // function citizenspeak_form

/**
 * Implementation of hook_insert().
 */
function citizenspeak_insert($node) {
  db_query("INSERT INTO {citizenspeak_campaigns} (nid, email_message, email_recipients, campaign_format) VALUES (%d, '%s', '%s', %d)", $node->nid, $node->email_message, $node->email_recipients, $node->campaign_format);
//  drupal_set_message(t('<p>The campaign "%title" has been saved and the following web address has been activated: %url</p><p>Pass the web address along to people you wish to join your campaign.</p><p>To monitor your campaign, please go to <a href="%url/report">%url/report</a></p><p>Good Luck!<br />%site_name</p>', array("%title" => $node->title, "%url" => url("node/". $node->nid, array('absolute' => true)), "%site_name" => variable_get("site_name", ""))));
} // function citizenspeak_insert

/**
 * Implementation of hook_load().
 */
function citizenspeak_load($node) {
  return db_fetch_array(db_query('SELECT * FROM {citizenspeak_campaigns} WHERE nid = %d', $node->nid)); 
} // function citizenspeak_load

/**
 * Implementation of hook_node_info().
 */
function citizenspeak_node_info() {
  return array(
    'citizenspeak' => array(
      'name' => t('CitizenSpeak'),
      'module' => 'citizenspeak',
      'description' => t('The CitizenSpeak module allows the creation of email petition campaign nodes. Users enter the email recipients and the a message to create a campaign. When someone visits the node, they see a form that displays a preview of the message and allows them to enter their contact information and a personal statement. When they submit the form, the target of the campaign is notified.')
    )
  );
}

/**
 * Implementation of hook_update().
 */
function citizenspeak_update($node) {
  db_query("UPDATE {citizenspeak_campaigns} SET email_message = '%s', email_recipients = '%s', campaign_format = %d WHERE nid = %d", $node->email_message, $node->email_recipients, $node->campaign_format, $node->nid);
} // function citizenspeak_update

/**
 * Implementation of hook_validate().
 *
 * @param &$node The node to be validated.
 */
function citizenspeak_validate(&$node) {
  if (isset($node->email_message) && $node->email_message == '') {
    form_set_error('email_message', 'You must provide an email message');
  }
  
  if (isset($node->email_recipients)) {
    if ($node->email_recipients == '') {
      form_set_error('email_recipients', 'You must provide at least one email address.');
    }
    
    $recipients = _citizenspeak_split_emails($node->email_recipients);
    
    if (count($recipients) > 25) {
      form_set_error('email_recipients', 'You may not have more than 25 email addresses.');
    }
    
    foreach ($recipients as $email) {
      if (!valid_email_address($email)) {
        form_set_error('email_recipients', 'You must provide valid email addresses.');
        break;
      }
    }
  } // if (isset($node->email_recipients))
} // function citizenspeak_validate


/**
 * Implementation of hook_view().
 *
 * @param &$node The node to be displayed.
 * @param $teaser Whether we are to generate a "teaser" or summary of the node,
 * rather than display the whole thing.
 * @param $page Whether the node is being displayed as a standalone page. If 
 * this is TRUE, the node title should not be displayed, as it will be printed 
 * automatically by the theme system. Also, the module may choose to alter the 
 * default breadcrumb trail in this case.
 * @return None. The passed-by-reference $node parameter should be modified as 
 * necessary so it can be properly presented by theme('node', $node).
 */

function citizenspeak_view($node, $teaser = FALSE, $page = FALSE){

  $node->body .= theme('citizenspeak_message_preview', $node);
  if (user_access('participate in campaigns')) {
    $node->body .= _citizenspeak_participate_display($node);
  }
  $node->teaser .= $node->email_message;

  $node->content['body']['#value'] = ($teaser) ? $node->teaser : $node->body;
  
  return $node;

} // function citizenspeak_view

/**
 * Display the form to participate in a campaign
 *
 * @param $node 
 *   A CitizenSpeak campaign node
 */
function _citizenspeak_participate_display($node) {
  return drupal_get_form('citizenspeak_send_petition_form');  
} // function _citizenspeak_form_display

function citizenspeak_send_petition_form(){
  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  );
  $form['organization'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization'),
    '#required' => FALSE,
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
  );
  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#required' => variable_get('citizenspeak_require_address', 1),
  );
  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#required' => variable_get('citizenspeak_require_address', 1),
  );
  
  $state_array = array('', 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming');
  $form['state'] = array(
    '#type' => 'select',
    '#title' => t('State'),
    '#options' => drupal_map_assoc($state_array),
    '#extra' => 0,
    '#multiple' => FALSE,
    '#required' => variable_get('citizenspeak_require_address', 1),
  );

  $form['zip'] = array(
    '#type' => 'textfield',
    '#title' => t('ZIP code'),
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => variable_get('citizenspeak_require_address', 1),
  );
  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#required' => FALSE,
  );
  $form['fax'] = array(
    '#type' => 'textfield',
    '#title' => t('Fax'),
    '#required' => FALSE,
  );
  $form['personal_statement'] = array(
    '#type' => 'textarea',
    '#title' => t('Personal statement'),
    '#cols' => 60,
    '#rows' => 20,
    '#description' => t('This will be included in the email sent.'),
  );
/*
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => 0,
  );
*/
  $form['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

function citizenspeak_form_alter(&$form, &$form_state, $form_id) {
  // put in the current node ID based on the arguments coming into the request.
  if($form_id == 'citizenspeak_send_petition_form'){
        $form_state['nid'] = arg(1);
  }
}
